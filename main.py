class ListNode(object):
    """Definition for singly-linked list."""
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class TreeNode(object):
    """Definition for a binary tree node."""
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution(object):
    def rotate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: None Do not return anything, modify nums in-place instead.
        """
        pass

    def mergeKLists(self, lists):
        """
        :type lists: List[ListNode]
        :rtype: ListNode
        """

    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        tmp = 0
        for num in nums:
            tmp ^= num

    def binaryGap(self, n):
        """
        :type n: int
        :rtype: int
        """
        pos = [i for i in range(64) if (n >> i) & 1]  # O(N)
        maximum = 0
        diff = 0
        for i in range(len(pos) - 1):
            diff = int(pos[i + 1]) - int(pos[i])
            if diff >= maximum:
                maximum = diff

    def canCross(self, stones):
        """
        :type stones: List[int]
        :rtype: bool
        """
        pass

    def isSameTree(self, p, q):
        """
        :type p: TreeNode
        :type q: TreeNode
        :rtype: bool
        """
        def in_order(p, q):
            if not p and not q:
                return True
            if not p or not q:
                return False
            print("p: %s q: %s" % (p.val, q.val))
            if p.val != q.val:
                return False
            else:
                return in_order(p.left, q.left) and in_order(p.right, q.right)

        return in_order(p, q)

    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        roman = {
            'I': 1,
            'V': 5,
            'X': 10,
            'L': 50,
            'C': 100,
            'D': 500,
            'M': 1000
        }
        value = 0
        prev = 0
        for c in s[::-1]:
            if roman.get(c) >= prev:
                value += roman.get(c)
            else:
                value += roman.get(c) - 2 * roman.get(c)
            prev = roman.get(c)
        return value

    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        hash_table = {}
        for i in range(len(nums)):
            val = hash_table.get(nums[i], -1)
            if val >= 0:
                return [val, i]
            else:
                hash_table.update({target - nums[i]: i})

    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        if x == 0 or x > pow(2, 31) - 1 or x < (-1) * pow(2, 31):
            return 0
        if x < 0:
            N = len(str(x)) - 2
        if x > 0:
            N = len(str(x)) - 1

        P = x
        stack = []
        while abs(P):
            m = abs(P) / pow(10, N)
            stack.append(int(m))
            P = abs(P) % pow(10, N)
            N -= 1

        reverse = 0 if len(stack) > 1 else stack.pop()
        M = 0
        while stack:
            m = stack.pop(0)
            reverse += m * pow(10, M)
            M += 1

        if x < 0:
            reverse *= -1

        if reverse > pow(2, 31) - 1 or reverse < (-1) * pow(2, 31):
            reverse = 0

        return reverse

    def partitionLabels(self, S):
        """
        :type S: str
        :rtype: List[int]
        """
        pass

    def reconstructQueue(self, people):
        """
        :type people: List[List[int]]
        :rtype: List[List[int]]
        """
        T = []
        for i in range(len(people) - 1):
            h, k = people[i]
            if [t >= h for t in T].count(True) != k:
                people[i + 1], people[i] = people[i], people[i + 1]
            else:
                T.append(h)

    def topKFrequent(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        table = {}
        result = set()
        for num in nums:
            val = table.get(num, None)
            if val:
                table.update({num: val + 1})
            else:
                table.update({num: 1})

        table = [k for k, v in sorted(table.items(), key=lambda item: item[1])]
        result = [table.pop() for i in range(k)]

        return result

    def reverseList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        stack = []
        result = point = ListNode(0)
        while head:
            stack.append(head.val)
            head = head.next

        while stack:
            point.next = ListNode(stack.pop())
            point = point.next

        return result.next

    def moveZeroes(self, nums):
        """
        Do not return anything, modify nums in-place instead.
        """
        A = []
        B = []

        for e in nums:
            if e:
                A.insert(0, e)
            else:
                B.append(e)

        for i in range(len(nums)):
            if A:
                nums[i] = A.pop()
            else:
                nums[i] = B.pop()
        return nums

    def majorityElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        table = {}
        n = len(nums)
        if n == 1:
            return nums[0]
        n = int(n / 2) if n % 2 == 0 else int((n + 1) / 2)
        for i in range(len(nums)):
            val = table.get(nums[i], 0)
            if val:
                if val >= (n - 1):
                    return nums[i]
                else:
                    table.update({nums[i]: val + 1})
            else:
                table.update({nums[i]: 1})

    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        c1, c2, exp = 0, 0, 0
        point = l1
        while point:
            c1 += (point.val * pow(10, exp))
            exp += 1
            point = point.next

        point = l2
        exp = 0
        while point:
            c2 += (point.val * pow(10, exp))
            exp += 1
            point = point.next

        c3 = c1 + c2
        exp = len(str(c3))
        stack = []

        while exp > 0:
            stack.append(int(c3 / pow(10, exp - 1)))
            c3 = int(c3 % pow(10, exp - 1))
            exp -= 1

        if not stack:
            return ListNode(0)

        head = point = ListNode(0)
        while stack:
            point.next = ListNode(stack.pop())
            point = point.next

        return head.next


def f1(s):
    hash_table = {}
    val = 0
    for i in range(len(s)):
        val = hash_table.get(s[i], None)
        if val is not None:
            return val
        else:
            hash_table.update({s[i]: i})


if __name__ == "__main__":
    leetcode = Solution()
    l1 = ListNode(5)
    l1.next = ListNode(6)
    l2 = ListNode(5)
    l2.next = ListNode(4)
    l2.next.next = ListNode(9)
    print(leetcode.addTwoNumbers(l1, l2))
